let lat = -25.4284
let lng = -49.2733
let x = 10
let url

const getPosition = (here) => {
    lat = here.coords.latitude
    lng = here.coords.longitude

    console.log(`Latitude : ${lat}`)
    console.log(`Longitude: ${lng}`)
}

url = `https://shrouded-mountain-15003.herokuapp.com/https://flickr.com/services/rest/?api_key=e6cd38fca7a5701bf6701980d958dd42&format=json&nojsoncallback=1&method=flickr.photos.search&safe_search=1&per_page=${x}&lat=${lat}&lon=${lng}&text=igreja`

const getPhotos = () => {
    fetch(url)
    .then(response => response.json())
    .then(data => {
        let c = 0

        const constructImageURL = (photoObj) => {
            return 'https://farm' + photoObj.farm + '.staticflickr.com/' + photoObj.server + '/' + photoObj.id + '_' + photoObj.secret + '.jpg'
        }

        let imageUrl = constructImageURL(data.photos.photo[c])
        let image = document.getElementById('image')
        image.src = imageUrl

        document.getElementById('pre').addEventListener('click', () => {
            if (c == 0) { 
                c = x - 1
                imageUrl = constructImageURL(data.photos.photo[c])
                image.src = imageUrl
            } else if (c > 0) {
                c--
                imageUrl = constructImageURL(data.photos.photo[c])
                image.src = imageUrl
            }
        })

        document.getElementById('for').addEventListener('click', () => {
            if (c == x - 1) { 
                c = 0
                imageUrl = constructImageURL(data.photos.photo[c])
                image.src = imageUrl
            } else if (c < x) {
                c++
                imageUrl = constructImageURL(data.photos.photo[c])
                image.src = imageUrl
            }
        })
    })
}

const getError = (err) => {
    console.warn(`ERROR(${err.code}): ${err.message}`, err)
    getPhotos()
}

navigator.geolocation.getCurrentPosition(getPhotos, getError)